package it.polito.po.test;
import static org.junit.Assert.*;

import java.util.*;
import org.junit.Before;
import org.junit.Test;

import operations.*;

public class TestR1 {
	Operations op = new Operations();
	int j = 0;
	String result;
	String expected;

	@Before
	public void setUp() {
		op = new Operations();
	}

	@Test
	public void testAddProductTypes1() throws OException {
		j = op.addProductType("sofa", 4, 300);

		assertEquals(1200, j);
	}

	@Test(expected = OException.class)
	public void testAddProductTypes2() throws OException {
		j = op.addProductType("table", 3, 100);
		j = op.addProductType("table", 2, 80);
	}

	@Test
	public void testGetNumberOfProducts1() throws OException {
		j = op.addProductType("sofa", 4, 300);
		j = op.getNumberOfProducts("sofa");

		assertEquals(4, j);
	}

	@Test(expected = OException.class)
	public void testGetNumberOfProducts2() throws OException {
		j = op.getNumberOfProducts("table");
	}

	@Test
	public void testGroupingProductTypesByPrices() throws OException {
		op.addProductType("sofa", 4, 300);
		op.addProductType("table", 2, 250);
		op.addProductType("desk", 2, 250);
		SortedMap<Integer, List<String>> map1 = op.groupingProductTypesByPrices();

		assertNotNull("Missing map grouping product types by prices", map1);
		assertEquals("{250=[desk, table], 300=[sofa]}", map1.toString());
	}
}
