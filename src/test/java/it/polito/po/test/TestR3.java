package it.polito.po.test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.util.*;
import org.junit.Before;
import org.junit.Test;

import operations.*;

public class TestR3 {
	Operations op = new Operations();
	int j = 0;
	String result;
	String expected;

	@Before
	public void setUp() throws OException {
		op = new Operations();
		op.addProductType("sofa", 4, 300);
		op.addProductType("table", 3, 100);
		op.addDiscount("customer1", 15);
		op.customerOrder("customer1", "sofa:1", 10);
	}

	@Test
	public void testEvalByCustomer1() throws OException {
		j = op.evalByCustomer("customer1", "sofa", 8);
		assertEquals(1, j);
	}

	@Test
	public void testEvalByCustomer2() throws OException {
		j = op.customerOrder("customer1", "table:2", 0);
		j = op.evalByCustomer("customer1", "sofa", 8);
		j = op.evalByCustomer("customer1", "table", 6);
		assertEquals(2, j);
	}

	@Test(expected = OException.class)
	public void testEvalByCustomer3() throws OException {
		j = op.evalByCustomer("customer1", "table", 8); // no purchase of table
	}

	@Test(expected = OException.class)
	public void testEvalByCustomer4() throws OException {
		j = op.evalByCustomer("customer1", "sofa", 8);
		j = op.evalByCustomer("customer1", "sofa", 6);
	}

	@Test(expected = OException.class)
	public void testEvalByCustomer5() throws OException {
		j = op.evalByCustomer("customer1", "sofa", 11);
	}

	@Test
	public void testGetScoreFromProductType1() throws OException {
		j = op.evalByCustomer("customer1", "sofa", 8);
		j = op.getScoreFromProductType("customer1", "sofa");
		assertEquals(8, j);
	}

	@Test(expected = OException.class)
	public void testGetScoreFromProductType2() throws OException {
		j = op.evalByCustomer("customer1", "sofa", 8);
		j = op.getScoreFromProductType("customer1", "table");
	}

	@Test
	public void testGroupingCustomersByScores1() throws OException {
		SortedMap<Integer, List<String>> map;
		op.addDiscount("customer2", 15);
		op.addDiscount("customer3", 15);

		j = op.customerOrder("customer3", "sofa:1", 10);
		j = op.customerOrder("customer2", "sofa:1", 5);
		j = op.evalByCustomer("customer3", "sofa", 7);
		j = op.evalByCustomer("customer2", "sofa", 9);

		map = op.groupingCustomersByScores("sofa");

		assertNotNull("Missing map with customers by scores", map);
		assertEquals("{7=[customer3], 9=[customer2]}", map.toString());
	}

	@Test
	public void testGroupingCustomersByScores2() throws OException {
		SortedMap<Integer, List<String>> map;
		j = op.addDiscount("customer3", 15);
		j = op.addDiscount("customer2", 10);
		j = op.addDiscount("customer1", 20);
		j = op.customerOrder("customer3", "table:1", 10);
		j = op.customerOrder("customer2", "table:1", 5);
		j = op.customerOrder("customer1", "table:1", 5);
		j = op.evalByCustomer("customer3", "table", 7);
		j = op.evalByCustomer("customer2", "table", 9);
		j = op.evalByCustomer("customer1", "table", 7);

		map = op.groupingCustomersByScores("table");

		assertNotNull("Missing map with customers by scores", map);
		assertEquals("{7=[customer1, customer3], 9=[customer2]}", map.toString());
	}
}
