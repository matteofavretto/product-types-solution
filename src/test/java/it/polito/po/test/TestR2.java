package it.polito.po.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import operations.OException;
import operations.Operations;

public class TestR2 {
	Operations op = new Operations();
	int j = 0;
	String result;
	String expected;
	
	@Test
	public void testAddDiscount1() {
		j = op.addDiscount("customer1", 15);

		assertEquals( 15, j);
	}

	@Test
	public void testAddDiscount2() {
		j = op.addDiscount("customer1", 15);
		j = op.addDiscount("customer1", 15);

		assertEquals( 30, j);
	}

	@Test
	public void testCustomerOrder1() throws OException {
		op.addProductType("sofa", 1, 300);
		op.addDiscount("customer1", 20);
		j = op.customerOrder("customer1", "sofa:1", 10);

		assertEquals( 290, j);
	}

	@Test // (expected=OException.class)
	public void testCustomerOrder2() throws OException {
		op.addProductType("sofa", 1, 300);
		op.addDiscount("customer1", 20);
		j = op.customerOrder("customer1", "sofa:2", 10); // only one sofa available
		j = op.getNumberOfProducts("sofa");
		
		assertEquals( 1, j);
	}

	@Test(expected=OException.class)
	public void testCustomerOrder3() throws OException {
		op.addProductType("sofa", 1, 300);
		op.addDiscount("customer1", 20);
		
		op.customerOrder("customer1", "sofa:1", 30);
	}

	@Test
	public void testCustomerOrder4() throws OException {
		op.addProductType("sofa", 1, 300);
		op.addDiscount("customer1", 20);
		j = op.customerOrder("customer1", "sofa:1", 20);
		
		assertEquals( 280,j );

		j = op.getNumberOfProducts("sofa");

		assertEquals( 0, j);
	}

	@Test
	public void testCustomerOrder5() throws OException {
		op.addProductType("sofa", 1, 300);
		op.addProductType("table", 2, 250);
		op.addDiscount("customer1", 20);
		op.addDiscount("customer1", 30);
		j = op.customerOrder("customer1", "sofa:1", 20);
		j = op.customerOrder("customer1", "table:1", 20);
		j = op.getDiscountAvailable("customer1");
		
		assertEquals(10, j);
	}

	@Test
	public void testCustomerOrder6() throws OException {
		op.addProductType("sofa", 1, 300);
		op.addProductType("table", 2, 250);
		op.addDiscount("customer1", 20);
		op.addDiscount("customer1", 30);
		j = op.customerOrder("customer1", "sofa:1 table:1", 20);
		j = op.getDiscountAvailable("customer1");
		
		assertEquals(30, j);
	}
}
