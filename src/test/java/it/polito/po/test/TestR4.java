package it.polito.po.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.SortedMap;

import org.junit.Before;
import org.junit.Test;

import operations.OException;
import operations.Operations;

public class TestR4 {
	
	Operations op = new Operations();
	int j = 0;
	String result;
	String expected;

	@Before
	public void setUp() throws OException {
		op = new Operations();
		op.addProductType("sofa", 4, 300);
		op.addProductType("table", 3, 100);
		op.addDiscount("customer1", 15);
		op.customerOrder("customer1", "sofa:1", 10);
	}
	
	@Test
	public void testGroupingCustomersByNumberOfProductsPurchased1() throws OException {
		SortedMap<Integer, List<String>> map;
		j = op.addDiscount("customer3", 15);
		j = op.addDiscount("customer2", 10);
		j = op.addDiscount("customer1", 20);
		j = op.customerOrder("customer3", "table:2", 10);
		j = op.customerOrder("customer2", "table:1", 5);
		j = op.customerOrder("customer1", "table:1", 5);

		map = op.groupingCustomersByNumberOfProductsPurchased();

		assertNotNull("Missing map with number of purchases", map);
		assertEquals("{1=[customer1, customer2], 2=[customer3]}", map.toString());
	}

	@Test
	public void testLargestExpenseForCustomer1() throws OException {
		SortedMap<String, Integer> map;
		j = op.addDiscount("customer3", 15);
		j = op.addDiscount("customer2", 10);
		j = op.addDiscount("customer1", 20);
		j = op.customerOrder("customer3", "table:2", 10);
		j = op.customerOrder("customer2", "table:1", 5);
		j = op.customerOrder("customer1", "table:1", 5);

		map = op.largestExpenseForCustomer();

		assertNotNull("Missing map with largest expenses", map);
		assertEquals("{customer1=290, customer2=95, customer3=190}", map.toString());
	}

}
