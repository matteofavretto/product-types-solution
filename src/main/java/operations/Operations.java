package operations;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.*;
import java.util.*;

public class Operations {

	SortedMap<String, ProductType> ptMap = new TreeMap<String, ProductType>();
	SortedMap<String, Customer> customerMap = new TreeMap<>();

//R1
	public int addProductType(String productType, int n, int price) throws OException {
		if (ptMap.containsKey(productType))
			throw new OException("dup pt " + productType);
		ptMap.put(productType, new ProductType(productType, n, price));
		return n * price;
	}

	public int getNumberOfProducts(String productType) throws OException {
		if (!ptMap.containsKey(productType))
			throw new OException("undef " + productType);
		return ptMap.get(productType).n;
	}

	public SortedMap<Integer, List<String>> groupingProductTypesByPrices() {
		return ptMap.values().stream().sorted(comparing(ProductType::getPTName))
				.collect(groupingBy(ProductType::getPTPrice, TreeMap::new, mapping(ProductType::getPTName, toList())));
	}

//R2
	public int addDiscount(String customer, int discount) {
		Customer c = customerMap.get(customer);
		if (c == null) {
			c = new Customer(customer);
			customerMap.put(customer, c);
		}
		c.totalDiscount += discount;
		return c.totalDiscount;
	}

	public int customerOrder(String customer, String ptpn, int discount) throws OException {
		List<String> productTypes = new ArrayList<>();
		List<Integer> numberOfProducts = new ArrayList<>();
		String[] subStrings = ptpn.split(" ");
		// System.out.println(Arrays.toString(subStrings));
		for (String s : subStrings) {
			// System.out.println(s);
			String[] subStrings2 = s.split(":");
			productTypes.add(subStrings2[0]);
			numberOfProducts.add(Integer.valueOf(subStrings2[1]));
		}
		// System.out.println(productTypes); System.out.println(numberOfProducts);

		// The sum of the discounts requested must be less than or equal
		// to the total discount received; otherwise, an exception is thrown.
		Customer c = customerMap.get(customer);
		if (c.discountsAlreadyUsed + discount > c.totalDiscount)
			throw new OException("wrong discount " + discount);

		int overallPrice = 0;
		int index = -1;
		int requiredN = 0;
		// If not all the numberOfProducts required are available the result is 0.
		for (String pt : productTypes) {
			index++;
			// System.out.println("index " + index);
			requiredN = numberOfProducts.get(index); // required number
			int availableN = ptMap.get(pt).n;
			if (availableN < requiredN)
				return 0;
		}
		index = -1;
		// The number of the products relating to a productType is reduced
		// by the number of products required by the customer order.
		for (String pt : productTypes) {
			index++;
			requiredN = numberOfProducts.get(index); // required number
			overallPrice += ptMap.get(pt).price * requiredN;
			ptMap.get(pt).n -= requiredN;
			c.numberOfProducts += requiredN; // usato per groupingCustomersByNumberOfProductsPurchased
		}

		// The result is the total price of the products to which the requested discount
		// is subtracted.
		c.addProductTypes(productTypes); // addPT to customer
		c.discountsAlreadyUsed += discount;
		if ((overallPrice - discount) > c.overallPriceForCustomerOrder)
			c.overallPriceForCustomerOrder = overallPrice - discount;

		return overallPrice - discount;
	}

	public int getDiscountAvailable(String customer) {
		// The result is the difference between the total discount received by the
		// customer
		// and the sum of the discounts already used.
		// The result is 0 if there is no discount that can be used.
		Customer c = customerMap.get(customer);
		return c.totalDiscount - c.discountsAlreadyUsed;
	}

//R3
	public int evalByCustomer(String customer, String productType, int score) throws OException {
		// An exception is thrown if the customer:
		// has not purchased any product of the type indicated,
		// has already given a score to the product type indicated, or the score is out
		// of range.
		Customer c = customerMap.get(customer);
		if (!c.mapPTScore.containsKey(productType))
			throw new OException("no purchase of " + productType);
		Integer existingScore = c.mapPTScore.get(productType);
		if (existingScore != null)
			throw new OException("existingScore " + existingScore);
		if (c.mapPTScore.get(productType) == null)
			c.mapPTScore.put(productType, score); // add score to product type
		if (score < 4 || score > 10)
			throw new OException("score out of range " + score);
		// ProductType pt = ptMap.get(productType); //prova
		// mapScoreCustomers.get
		return c.numberOfScores();
	}

	public int getScoreFromProductType(String customer, String productType) throws OException {
		Customer c = customerMap.get(customer);
		if (!c.mapPTScore.containsKey(productType))
			throw new OException("no score of " + productType + " by " + c.name);
		int existingScore = c.mapPTScore.get(productType);
		return existingScore;
	}

	public SortedMap<Integer, List<String>> groupingCustomersByScores(String productType) {
		ArrayList<ScoresCustomers> listOf2elements = new ArrayList<>();

		for (Customer c : customerMap.values()) {
			if (c.mapPTScore.keySet().contains(productType)) {
				Integer score = c.mapPTScore.get(productType);
				if (score != null) {
					listOf2elements.add(new ScoresCustomers(score, c.name));
				}
			}
		}
		// System.out.println(listOf2elements);
		return listOf2elements.stream().collect(
				groupingBy(ScoresCustomers::getScore, TreeMap::new, mapping(ScoresCustomers::getCustomer, toList())));

	}

//R4
	public SortedMap<Integer, List<String>> groupingCustomersByNumberOfProductsPurchased() {
		return customerMap.values().stream().filter(c -> c.getNofProducts() > 0)
				.collect(groupingBy(Customer::getNofProducts, TreeMap::new, mapping(Customer::getName, toList())));

	}
//groups the customers by increasing number of products purchased. 
//The customers are listed in alphabetical order.

	public SortedMap<String, Integer> largestExpenseForCustomer() {
		return customerMap.values().stream().filter(c -> c.getMaxOverallPrice() > 0)
				.collect(toMap(Customer::getName, Customer::getMaxOverallPrice, (p1, p2) -> p1, TreeMap::new));
	}
//provides the largest expense for each customer (in increasing order)

}
