package operations;

class ScoresCustomers {
	Integer score;
	String customer;
	ScoresCustomers (Integer score, String customer) {
		this.score = score; this.customer = customer;
	}
	Integer getScore () {return score;}
	String getCustomer () {return customer;}
	public String toString () {return score + " " + customer;}
}
