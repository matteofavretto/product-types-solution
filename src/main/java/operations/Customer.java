package operations;

import java.util.*;

class Customer {
	String name;
	int totalDiscount;
	int discountsAlreadyUsed;
	SortedMap<String, Integer> mapPTScore = new TreeMap<>();

	Customer(String name) {
		this.name = name;
	}

	String getName() {
		return name;
	}

	void addProductTypes(List<String> list) {
		for (String pt : list) {
			if (!mapPTScore.containsKey(pt))
				mapPTScore.put(pt, null);
		}
	}

	int numberOfScores() {
//		int n = 0;
//		for (Integer score : mapPTScore.values()) {
//			if (score != null)
//				n++;
//		}
//		return n;
		return (int) mapPTScore.values().stream().filter( s -> s != null ).count();

		
	}

	int numberOfProducts; // usato per groupingCustomersByNumberOfProductsPurchased

	int getNofProducts() {
		return numberOfProducts;
	}

	int overallPriceForCustomerOrder;

	int getMaxOverallPrice() {
		return overallPriceForCustomerOrder;
	}
}
